# LoViTra

My Bachelor-Thesis implementation.
This project translates a given Lola specification into a Viper program.
The Viper program then can be verified with the Viper backend.

Explicit Information about the contents and functionality can be found in the thesis document.

## Prerequisites

Cargo is required to compile and/or execute the code. Cargo will handle all Rust package dependencies.
To self compile the system access to the StreamLAB backend code is required.
(The implementation uses the StreamLAB parser and data structures)

To verify the output file the Viper backend is needed.
This can be either done by the standalone executable of the [Viper Backend](https://www.pm.inf.ethz.ch/research/viper/downloads.html) or the Viper Visual Studio Code plugin.
This can be obtained in the VSC extension manager.

## Use and Parameters

The executable can be given three kind of arguments.

An example call can look like this:
```
./target/release/lovitra --c --i 3 ./input/example_spec.lola
```

The call would result in the output of a new file:
```
./input/example_spec-3.vpr
```
which contains the new Viper program and can be used for verification.

### Parameters

If the parameter `--c` is given, the monitor behaviour in a concurrent context is modeled and all function computations in the loop are extracted into functions

If the parameter `--i n` is given and n is a positive number, the iteration depth for expression inlining is bounded to `n`.
Omitting this parameter or providing any negative value results in an `n` of infinity.

Every other argument is interpreted as a file path and passed to the translation in a separate thread.

Please provide all functional arguments before any file paths.

## Authors

* **Stefan Oswald** - *Initial work* - [Nekronod](https://gitlab.com/Nekronod)

## License

MIT License.

## Acknowledgments

* Thanks to the chair of Reactive Systems at the UDS.
* Thanks Max and Noemi for their advising.



