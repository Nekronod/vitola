use front::ir::{
    ArithLogOp::*, Constant, Expression, ExpressionKind, ExpressionKind::*, LolaIR, Offset,
    Offset::*, StreamReference,
};

use crate::stream::OffsetStream;

use super::*;
use crate::function::get_function;

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub struct Context<'a> {
    pub input_stream: Vec<OffsetStream>, //hold input stream references
    pub output_stream: Vec<OffsetStream>, // hold output stream references
    pub trigger: Vec<OffsetStream>,      //Trigger expressions
    pub divisor: Vec<String>,              //expressions present as a divisor
    functions: Vec<&'static str>,
    inline: bool,
    ir: &'a LolaIR,
    default: Option<String>, //used for default computation -gets Some(_) in default for rec call else None
    iteration: i32,          // what iteration we want to compute
    shift: i32,              //shift of the current stream
    state: State,
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum State {
    PRE,
    LOOP,
    POST,
    FUNCTION,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Mode {
    INV,
    FUNC,
    DEP,
}

impl<'a> Context<'a> {
    pub fn new(
        input: Vec<OffsetStream>,
        output: Vec<OffsetStream>,
        trigger: Vec<OffsetStream>,
        ir: &'a LolaIR,
    ) -> Self {
        Self {
            input_stream: input.clone(),
            output_stream: output.clone(),
            trigger: trigger.clone(),
            functions: Vec::new(),
            divisor: Vec::new(),
            inline: false,
            ir,
            default: None,
            iteration: 0,
            shift: 0,
            state: State::PRE,
        }
    }

    pub fn set_shift(&mut self, s: i32) {
        self.shift = s;
    }

    pub fn get_shift(&mut self) -> i32{
        self.shift
    }
    pub fn set_iteration(&mut self, i: i32) {
        self.iteration = i;
    }
    pub fn set_inline(&mut self, b: bool) {
        self.inline = b;
    }

    pub fn set_func(&mut self) {
        self.state = State::FUNCTION;
    }
    pub fn set_pre(&mut self) {
        self.state = State::PRE;
    }
    pub fn set_loop(&mut self) {
        self.state = State::LOOP;
    }
    pub fn set_post(&mut self) {
        self.state = State::POST;
    }

    fn is_loop(&self) -> bool {
        match self.state {
            State::LOOP => true,
            _ => false,
        }
    }
    fn is_post(&self) -> bool {
        match self.state {
            State::POST => true,
            _ => false,
        }
    }

    pub fn get_functions(&self) -> Vec<&str> {
        self.functions.clone()
    }

    pub fn get_stream(&self, s_r: StreamReference) -> &OffsetStream {
        match s_r {
            StreamReference::InRef(x) => &self.input_stream[x],
            StreamReference::OutRef(x) => &self.output_stream[x],
        }
    }

    pub fn print_expression(&mut self, expr: &Expression, depth: i32) -> String {
        match &expr.kind {
            LoadConstant(c) => match c {
                Constant::Str(x) => x.to_string(),
                Constant::Bool(b) => {
                    if *b {
                        "true".to_string()
                    } else {
                        "false".to_string()
                    }
                }
                Constant::UInt(u) => u.to_string(),
                Constant::Int(i) => i.to_string(),
                Constant::Float(f) => {
                    let mut multiplier: u32 = 0;
                    let mut tmp: f64 = *f;
                    while tmp - tmp.floor() > 0.0 {
                        tmp *= 10.0;
                        multiplier += 1;
                    };
                    format!("{}/{}",tmp.to_string(),10_i64.pow(multiplier))
                },
            },
            ArithLog(op, expr, _) => {
                let op_string: String = match op {
                    Not => "!",
                    Add => "+",
                    Sub => "-",
                    Mul => "*",
                    Div => "/",
                    Rem => "%",
                    Pow => unimplemented!("TODO pow"), //TODO power "**"
                    And => "&&",
                    Or => "||",
                    Eq => "==",
                    Lt => "<",
                    Le => "<=",
                    Ne => "!=",
                    Ge => ">=",
                    Gt => ">",
                    _ => unreachable!(),
                }
                .to_string();
                if expr.len() == 1 {
                    format!("{}{}", op_string, self.print_expression(&expr[0], depth ))
                } else if expr.len() == 2 {
                    let val2_str = self.print_expression(&expr[1], depth );
                    if let Div = op {
                        if let LoadConstant(x) = &expr[1].kind {

                        } else {
                        self.divisor.push(val2_str.clone());
                        }
                    };
                    format!(
                        "({} {} {})",
                        self.print_expression(&expr[0], depth ),
                        op_string,
                        val2_str,
                    )
                } else {
                    " TODO array ".to_string()
                }
            }
            OffsetLookup {
                target: reference,
                offset: off,
            } => self.match_stream_access(reference, off, depth ),
            StreamAccess(reference, kind) => {
                match kind {
                    front::ir::StreamAccessKind::Sync => {}
                    _ => unimplemented!("Async stream access"),
                };
                self.match_stream_access(reference, &FutureDiscreteOffset(0), depth )
            }
            Ite {
                condition: cond,
                consequence: cons,
                alternative: alt,
            } => format!(
                "(({})? {} : {})",
                self.print_expression(cond, depth),
                self.print_expression(cons, depth),
                self.print_expression(alt, depth)
            ),
            Default {
                expr: expression,
                default: def,
            } => {
                assert!(
                    match expression.kind {
                        OffsetLookup { .. } => true,
                        _ => false,
                    },
                    "expect offset below default"
                );
                let def_string = self.print_expression(def, depth);
                self.default = Some(def_string);
                let ret = self.print_expression(expression, depth);
                self.default = None;
                ret
            }
            ExpressionKind::Function(name, args, typ) => {
                self.functions.push(get_function(name));
                format!("{}({})",
                        name,
                        args
                            .iter()
                            .map(|exp| self.print_expression(exp,depth))
                            .collect::<Vec<String>>()
                            .join(", "))
            } ,
            _ => unreachable!("Function call not supported"),
        }
    }

    fn get_off_stream(&'a self, reference: &StreamReference) -> &'a OffsetStream {
        match reference {
            front::ir::StreamReference::OutRef(o) => &self.output_stream[*o],
            front::ir::StreamReference::InRef(o) => &self.input_stream[*o],
        }
    }

    fn match_stream_access(&mut self, reference: &StreamReference, off: &Offset, depth: i32) -> String {
        let off_stream: &OffsetStream = self.get_off_stream(reference);
        let s_ref = off_stream.get_stream_ref();
        let input = off_stream.input;
        let name = if input {
            &self.ir.get_in(s_ref).name
        } else {
            &self.ir.get_out(s_ref).name
        };
        let offset = off_stream.get_access_point(self.shift, *off);
        let idx = off_stream.memory_size + offset;
        //dbg!(self.iteration, offset, idx, *off, name, self.shift, off_stream.memory_size, &self.state, self.inline);
        /*
        println!("\n");//dbg
        println!("\n");//dbg
        */
        if self.is_post() {
            //println!("Post!");
            if self.shift - offset + self.iteration >= 0
                && idx <= off_stream.memory_size - (-self.iteration - off_stream.shift)
            {
                let mut missing_mem = -self.iteration - (off_stream.shift + 1);
                missing_mem = if missing_mem < 0 { 0 } else { missing_mem };
                return match reference {
                    StreamReference::InRef(_) => {
                        //TODO FIXME paper graph postfix has negative access
                        if self.inline {
                            format!("{}[N - {}]", name, - offset + self.iteration + 1)
                        } else {
                            //dbg!(self.iteration, offset, idx, *off, name, self.shift, off_stream.memory_size, &self.state, self.inline);
                            format!("r.{}_mem[{}]", name, idx)
                        }
                    },
                    StreamReference::OutRef(x) => {
                        if offset == 0 && depth != 0 {
                            if self.inline {
                                let old_shift = self.shift;
                                self.shift = off_stream.shift;
                                let res  = self.print_expression(&self.ir.outputs[*x].expr, depth -1);
                                self.shift = old_shift;
                                res
                            } else {
                                name.to_string()
                            }
                        } else {
                            //TODO FIXME paper graph postfix has negative access
                            if self.inline {
                                format!("{}_ghost[N - {}]", name, off_stream.shift + self.iteration - offset + 1)
                            } else {
                                use std::cmp::max;
                                format!("r.{}_mem[{}]", name, max(idx - missing_mem, 0))
                            }
                        }
                    } //0 Offset results in current access
                };
            } else {
                return self.default.as_ref().unwrap().clone();
            }
        }
        assert!(!self.is_post());
        let span = off_stream.memory_size + off_stream.shift;
        if self.iteration + off_stream.memory_size < self.shift && !self.is_loop()
            || self.iteration < span && idx - (span - self.iteration) < 0
        {
            self.default.as_ref().unwrap().clone()
        } else {
            match reference {
                StreamReference::InRef(_) => {
                    if idx == off_stream.memory_size {
                        if self.state == State::LOOP || self.state == State::FUNCTION {
                            if self.inline {
                                format!("{}[{}]", name, "i - 1")
                            } else if self.state == State::FUNCTION {
                                format!("{}", name)
                            } else {
                                format!("{}[{}]", name, "i")
                            }
                        } else {
                            format!("{}[{}]", name, self.iteration)
                        }
                    } else {
                        if self.state != State::LOOP && self.inline{
                            return format!("{}[{}]", name, self.iteration - (off_stream.memory_size - idx));
                        }
                        if self.inline {
                            return format!("{}[i - {}]", name, off_stream.memory_size - idx + 1);
                        }
                        let span = off_stream.memory_size + off_stream.shift;
                        if self.iteration < span {
                            let midx = idx - (span - self.iteration);
                            format!("r.{}_mem[{}]", name, midx)
                        } else {
                            format!("r.{}_mem[{}]", name, idx)
                        }
                    }
                }
                StreamReference::OutRef(x) => {
                    if offset == 0 && depth != 0{
                        if !self.inline {
                            name.to_string()
                        } else {
                            //println!("INLINED!\n");//dbg
                            let old_shift = self.shift;
                            self.shift = off_stream.shift;
                            let res  = self.print_expression(&self.ir.outputs[*x].expr, depth - 1);
                            self.shift = old_shift;
                            res
                        }
                    } else if self.inline && self.state == State::LOOP{
                        format!("{}_ghost[i - {}]", name, -offset + off_stream.shift + 1)
                    } else if self.inline && self.state != State::LOOP{
                        format!("{}_ghost[{}]", name,self.iteration - (-offset + off_stream.shift))
                    } else if self.iteration < span {
                        format!("r.{}_mem[{}]", name, idx - (span - self.iteration))
                    } else {
                        format!("r.{}_mem[{}]", name, idx)
                    }
                }
            }
        }
    }

    pub fn print_exp_by_idx(&mut self, idx: usize, depth: i32) -> String {
        self.print_expression(&self.ir.get_out(self.output_stream[idx].stream_ref).expr, depth)
    }

    pub fn get_inline_dep(&self, sr: &StreamReference, depth: i32, mode: Mode) -> Vec<OffsetStream> {
        let mut res: Vec<OffsetStream> = Vec::new();
        let s = match sr {
            StreamReference::OutRef(x) => self.output_stream[*x].clone(),
            StreamReference::InRef(x) => self.input_stream[*x].clone(),
        };
        if mode == Mode::INV && s.input {
            res.push(s);
        } else {
            for dep in self.ir.get_out(s.stream_ref).outgoing_dependencies.iter() {
                let target = match dep.stream {
                    StreamReference::OutRef(x) => self.output_stream[x].clone(),
                    StreamReference::InRef(x) => self.input_stream[x].clone(),
                };
                for access in &dep.offsets {
                    let edge_weight = match access {
                        Offset::FutureDiscreteOffset(x) => -(*x as i32),
                        Offset::PastDiscreteOffset(x) => *x as i32,
                        _ => unimplemented!("inline non discrete offset"),
                    };
                    if s.shift + edge_weight == target.shift  && depth != 0 {
                        if mode == Mode::DEP {
                            continue;
                        }
                        let rec = self.get_inline_dep(&dep.stream, depth - 1, mode.clone());
                        for stream in &rec {
                            res.push(stream.clone());
                        }
                    } else {
                        if mode == Mode::FUNC && s.shift + edge_weight != target.shift{
                            continue;
                        }
                        res.push(target.clone());
                    }
                }
            }
        }
        res.sort();
        res.dedup();
        res
    }
}
