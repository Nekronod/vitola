static FUNCTION_MAX :&str = "\
function max(a: Int, b:Int): Int {\
a < b ? b : a\
}";

static FUNCTION_MIN :&str = "\
function max(a: Int, b:Int): Int {\
a < b ? a : b\
}";

static FUNCTION_ABS :&str = "\
function abs(a: Int): Int {\
a < 0 ? -a : a\
}";

static FUNCTION_SIN :&str = "\
function sin(x: Int): Int\n\
    ensures result >= -1 && result <= 1\n\
    ensures x == 0 ==> result == 0\n\
    ensures forall i:Int,j:Int:: i == j ==> sin(i) == sin(j)\n\
 ";

static FUNCTION_COS :&str = "\
function cos(x: Int): Int\n\
    ensures result >= -1 && result <= 1\n\
    ensures x == 0 ==> result == 1\n\
    ensures forall i:Int,j:Int:: i == j ==> sin(i) == sin(j)\n\
 ";

pub fn get_function (n: &str) -> &'static str{
    match n {
        "sin" => FUNCTION_SIN,
        "cos" => FUNCTION_COS,
        "abs" => FUNCTION_ABS,
        "max" => FUNCTION_MAX,
        "min" => FUNCTION_MIN,
        _ => unimplemented!("unknown function called"),
    }
}
