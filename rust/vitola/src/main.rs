extern crate matrix;
extern crate streamlab_frontend as front;

mod expr;
mod shift;
mod function;
mod stream;
#[cfg(test)]
mod tests;

use crate::expr::{Context, Mode};
use crate::shift::Graph;
use crate::stream::{from_input, from_output, OffsetStream};
use front::ir::{InputStream, LolaIR, StreamReference, Type, Type::*};
use std::error::Error;
use std::fs::File;
use std::io::{Read, Write};
use std::option::Option;
use std::path::Path;
use std::string::String;
use std::{env, thread};

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Require at least one input specification file!")
    } else {
        let mut threads = Vec::new();
        let mut concurrency = false;
        let mut skip = false;
        let mut depth = -1;
        for idx in 1..args.len() {
            let input: String = args[idx].to_owned();
            if let "--c" = &*input {
                concurrency = true;
                continue;
            }

            if let "--i" = &*input {
                let next = &args[idx+1];
                match next.parse() {
                    Ok(i) => depth = i,
                    Err(_) => panic!("Could not parse arg after --i as int")
                };
                skip = true;
                continue;
            }
            if skip {
                skip = false;
                continue;
            }
            let mut s = "".to_string();
            for c in input.chars() {
                if c == '/' {
                    s = "".to_string();
                } else {
                    s.push(c);
                }
            }
            let path = Path::new(&input);
            let display = path.display();
            let mut file = match File::open(&path) {
                Err(why) => panic!("couldn't open {}: {}", display, why.description()),
                Ok(f) => f,
            };
            let mut prog = String::new();
            match file.read_to_string(&mut prog) {
                Err(why) => panic!("couldn't read {}: {}", display, why.description()),
                Ok(_) => (),
            }

            let name = s.clone();
            let p = prog.clone();
            let t = thread::spawn(move || {
                let ir = front::parse(&name, &p, front::FrontendConfig::default());
                let vpr = match ir {
                    Err(why) => panic!("parsing error: {}", why), //TODO
                    Ok(i) => translate_depth(&i,depth, concurrency),
                };

                let out_name = if depth == -1 {format!("{}.vpr", &input[0..input.len()-5])}
                    else {format!("{}-{}.vpr", &input[0..input.len()-5],depth)};
                let out_path = Path::new(&out_name);
                let out_display = out_path.display();
                let mut out_file = match File::create(&out_path) {
                    Err(why) => panic!("couldn't create {}: {}", out_display, why.description()),
                    Ok(f) => f,
                };
                match out_file.write_all(vpr.as_bytes()) {
                    Err(why) => panic!("couldn't write {}: {}", out_display, why.description()),
                    Ok(_) => println!("Successfully wrote to {}", out_display),
                }
            });
            threads.push(t);
        }
        for child in threads {
            let _ = child.join();
        }
    }
}

#[derive(Debug)]
struct Transpiler<'a> {
    ir: &'a LolaIR,
    ctx: Context<'a>,
    inputs: Vec<OffsetStream>,
    outputs: Vec<OffsetStream>,
    memory_stream: Vec<OffsetStream>,
    trigger: Vec<OffsetStream>,
    max_shift: i32,
    max_length: i32,

    result_str: String,
}

impl Transpiler<'_> {
    pub fn new(ir: &'static LolaIR) -> Self {
        let graph: Graph = Graph::new(ir);
        let shift_size: Vec<(u32, u32)> = graph.shift();
        let layer: Vec<i32> = graph.layer(shift_size.iter().map(|s|s.0).collect());
        let (m_s, m_l) = shift_size.iter().map(|(s, m)| (*s, s + m)).max().unwrap();
        let mut t = Transpiler {
            ir,
            ctx: Context::new(Vec::new(), Vec::new(), Vec::new(), ir),
            inputs: ir
                .inputs
                .iter()
                .enumerate()
                .map(|(idx, i)| from_input(i, shift_size[idx].0 as i32))
                .collect(),
            outputs: ir
                .outputs
                .iter()
                .enumerate()
                .map(|(idx, i)| {
                    from_output(
                        i,
                        shift_size[idx + ir.inputs.len()].0 as i32,
                        shift_size[idx + ir.inputs.len()].1 as i32,
                        0,
                    )
                })
                .collect(),
            memory_stream: Vec::new(),
            trigger: Vec::new(),
            max_shift: m_s as i32,
            max_length: m_l as i32,
            result_str: "".to_string(),
        };
        let mut mem = Vec::new();
        t.inputs
            .iter()
            .chain(t.outputs.iter())
            .filter(|s| s.memory_size > 0)
            .for_each(|s| mem.push(s.clone()));

        for tr in &ir.triggers {
            if let StreamReference::OutRef(x) = tr.reference {
                t.trigger.push(t.outputs[x].clone());
            }
        }
        t.ctx = Context::new(t.inputs.clone(), t.outputs.clone(), t.trigger.clone(), t.ir);
        t
    }

    fn append_str(&mut self, s: &str) {
        self.result_str.push_str(s);
    }

    pub fn get_result(&self) -> String {
        self.result_str.clone()
    }
}

pub fn translate(ir: &front::ir::LolaIR, concurrency: bool) -> String{
    translate_depth(ir,-1, concurrency)
}


pub fn translate_depth(ir: &front::ir::LolaIR, inline_depth :i32, layer_optimization: bool) -> String {


    let input_refs: &Vec<usize> = &ir.input_refs();
    let output_refs: &Vec<usize> = &ir.output_refs();
    let mut input: Vec<&InputStream> = Vec::new();
    let mut output = Vec::new();

    for index in input_refs {
        input.push(ir.get_in(StreamReference::InRef(*index)));
    }

    for index in output_refs {
        output.push(ir.get_out(StreamReference::OutRef(*index)));
    }

    let mut graph: Graph = Graph::new(ir);
    let shift_size: Vec<(u32, u32)> = graph.shift();
    let layer: Vec<i32> = graph.layer(shift_size.iter().map(|s|s.0).collect());

    let mut input_streams: Vec<OffsetStream> = Vec::new();
    let mut output_streams: Vec<OffsetStream> = Vec::new();
    let mut trigger = Vec::new();
    for (i, in_stream) in input.iter().enumerate() {
        input_streams.push(OffsetStream {
            input: true,
            idx: match in_stream.reference { StreamReference::InRef(x) => x, _ => unreachable!("invalid Streamref Match - init")},
            shift: 0, //TODO
            trigger: false,
            memory_size: shift_size[i].1 as i32, //FIXME
            layer:0,
            stream_ref: in_stream.reference,
            t: in_stream.ty.clone(),
        });
    }
    for (i, out_stream) in output.iter().enumerate() {
        output_streams.push(OffsetStream {
            input: false,
            idx: match out_stream.reference { StreamReference::OutRef(x) => x, _ => unreachable!("invalid Streamref Match - init")},
            shift: shift_size[i + ir.inputs.len()].0 as i32, //TODO
            trigger: false,
            memory_size: shift_size[i + ir.inputs.len()].1 as i32,
            layer:layer[i + ir.inputs.len()],
            stream_ref: out_stream.reference,
            t: out_stream.ty.clone(),
        });
    }

    for trigg in &ir.triggers {
        if let StreamReference::OutRef(idx) = trigg.reference {
            let mut t = &mut (output_streams[idx]);
            t.trigger = true;
        }
    }
    for maybe_trigger in &output_streams {
        if maybe_trigger.trigger {
            assert!(maybe_trigger.memory_size == 0);
            trigger.push(maybe_trigger.clone());
        }
    }


    let mut ctx = expr::Context::new(input_streams, output_streams, trigger, ir);

    let mut memory_stream = Vec::new();
    for o_stream in ctx
        .input_stream
        .clone()
        .iter()
        .chain(ctx.output_stream.clone().iter())
    {
        if o_stream.memory_size > 0 {
            memory_stream.push(o_stream.clone());
        }
    }
    let mut dep_per_trigger: Vec<Vec<OffsetStream>> = Vec::new();
    for t in &ctx.trigger {
        let dep: Vec<OffsetStream> = ctx.get_inline_dep(&t.stream_ref, inline_depth, Mode::INV);
        dep_per_trigger.push(dep);
    }
    let mut trigger_deppending: Vec<OffsetStream> = Vec::new();
    for v in &dep_per_trigger {
        for s in v.iter() {
            trigger_deppending.push(s.clone());
        }
    }
    trigger_deppending = trigger_deppending
        .into_iter()
        .filter(|s| !s.input)
        .collect::<Vec<OffsetStream>>();
    trigger_deppending.sort();
    trigger_deppending.dedup();

    // Memory length field array init.
    let mut field_init = Vec::new();
    let mut field_name = Vec::new();
    for m in &memory_stream {
        if m.memory_size < 1 {
            continue;
        }
        let n = m.get_name(ir);
        let t = type_string(&m.t);
        field_init.push(format!("field {}_mem :Seq[{}]\n", n, t));
        field_name.push(format!("{}_mem", n))
    }


    let mut result_string = "method monitor (".to_string();

    let prefix_length = shift_size.iter().map(|p| p.0 + p.1).max().unwrap() as i32 + 1; //FIXME
    let max_shift = shift_size.iter().map(|p| p.0).max().unwrap() as i32;
    assert!(prefix_length >= max_shift);

    let mut layered_streams = Vec::new();
    if layer_optimization {
        layered_streams = ctx.output_stream.clone();
        layered_streams.sort_by(|s1, s2| s1.layer.cmp(&s2.layer));

        ctx.set_func();
        ctx.set_iteration(prefix_length);

        for out  in ctx.output_stream.clone().iter() {
            let name = out.get_name(ir);
            let mut dependency_list = ctx.get_inline_dep(&ctx.output_stream[out.idx].stream_ref, 0, Mode::FUNC);
            let arguments: Vec<String> = dependency_list.iter().map(|oss| oss.get_name(ir)).collect();
            let types: Vec<String> = dependency_list.iter().map(|oss| type_string(&oss.t)).collect();
            let param_string: Vec<String> = arguments.iter().zip(types).map(|(name,t)| format!("{}: {}",name,t)).collect();
            let mut memory_list = ctx.get_inline_dep(&ctx.output_stream[out.idx].stream_ref, 1, Mode::DEP);

            let mut function_header = format!(
                "function compute_{}({}{}{}): {}\n",
                name,
                if memory_list.len() > 0 {"r: Ref"} else {""}, //TODO check
                if memory_list.len() > 0  && param_string.len() > 0 {", "} else {""}, //TODO check
                param_string.join(", "),
                type_string(&out.t),
            );

            let mut function_inv = "".to_string();
            for oss in memory_list {
                let n = oss.get_name(ir);
                function_inv+= &format!("\trequires acc(r.{}_mem, 1/{})\n",n, ctx.output_stream.len());
                function_inv+= &format!("\trequires |r.{}_mem| == {}\n",n, oss.memory_size);
            }

            ctx.set_shift(out.shift);
            let mut function_body = ctx.print_exp_by_idx(out.idx, inline_depth);

            let function_string = format!("{}{}{{\n\t{}\n}}\n\n", function_header, function_inv, function_body);

            result_string = function_string + &result_string;
        }
        ctx.set_pre();
    }

    // t1: Seq[Int], t2: Seq[Bool], .... )\n
    let args: Vec<String> = ctx
        .input_stream
        .iter()
        .map(|s| format!("{}: Seq[{}]", s.get_name(ir), type_string(&s.t)))
        .collect();
    result_string = format!(
        "{}{}{})\n",
        field_init.join(""),
        result_string,
        args.join(", ")
    );

    //invariant |t1| == |t2|
    let mut inv_length: Vec<String> = Vec::new();

    if ctx.input_stream.len() == 1 {
    } else {
        let mut first = ctx.input_stream[0].get_name(ir);
        for i in ctx.input_stream.iter().skip(1) {
            let next_string = i.get_name(ir);
            inv_length.push(format!("\trequires |{}| == |{}|\n", first, next_string));
            first = next_string;
        }
    }

    let inv_length_min = format!(
        "\trequires |{}| >= {}\n{{\n",
        ctx.input_stream[0].get_name(ir),
        prefix_length
    );
    result_string.push_str(&format!("{}{}", inv_length.join(""), inv_length_min));
    result_string.push_str(&format!(
        "\tvar N: Int := |{}|\n",
        ctx.input_stream[0].get_name(ir)
    ));
    if !field_name.is_empty() {
    result_string.push_str(&format!(
        "\tvar r: Ref\n\tr := new({})\n",
        field_name.join(", ")
    ));
        result_string.push_str(&format!(
            "\tr.{} := Seq()\n\n",
            field_name.join(" := Seq()\n\tr.")
        ));
    }
    result_string.push_str(
        &ctx.output_stream
            .iter()
            .map(|o| format!("\tvar {}: {}\n", o.get_name(ir), type_string(&o.t)))
            .collect::<Vec<String>>()
            .join(""),
    );

    //GM init
    result_string.push_str(
        &trigger_deppending
            .iter()
            .chain(ctx.trigger.iter())
            .map(|o| {
                format!(
                    "\tvar {}_ghost: Seq[{}] := Seq()\n",
                    o.get_name(ir),
                    type_string(&o.t)
                )
            })
            .collect::<Vec<String>>()
            .join(""),
    );
    //GM

    result_string.push_str("\n");

    //PREFIX
    for iteration in 0..prefix_length {
        result_string.push_str(&format!("\n\t// Iteration {}\n", iteration));
        ctx.set_iteration(iteration);
        for idx in 0..ir.outputs.len() {
            let shift = shift_size[idx + ir.inputs.len()].0 as i32;
            ctx.set_shift(shift);
            if shift > iteration {
                continue;
            }
            let exp_result = ctx.print_exp_by_idx(idx, inline_depth);
            for divisor in &ctx.divisor {
                result_string.push_str(&format!(
                    "\t assume 0 != {}\n",
                    divisor,
                ))
            };
            ctx.divisor.clear();
            result_string.push_str(&format!(
                "\t{} := {}\n",
                ir.outputs[idx].name.clone(),
                exp_result,
            ))
        }
        result_string.push_str("\n");

        //Update memory
        result_string.push_str(&memory_update(&ctx.input_stream, ir, iteration, false));
        result_string.push_str(&memory_update(&ctx.output_stream, ir, iteration, false));

        result_string +="\n";
        result_string.push_str(&ghost_update(&trigger_deppending, ir, iteration, false));
        result_string.push_str(&ghost_update(&ctx.trigger, ir, iteration, false));


        ctx.set_inline(true);
        result_string +="\n";
        for idx in 0..ctx.trigger.len() {
            let tr = ctx.trigger[idx].clone();
            ctx.set_shift(tr.shift);
            if ctx.get_shift() > iteration {
                continue;
            }
            result_string.push_str(&format!(
                "\tassert {}_ghost[{}] == {}\n",
                tr.get_name(ir),
                iteration - tr.shift,
                ctx.print_exp_by_idx(tr.idx, inline_depth)
            ))

        }
        ctx.set_inline(false);
    }

    //LOOP Header
    result_string.push_str(&format!("\n\tvar i: Int := {}\n", prefix_length));
    result_string.push_str(&"\twhile( i < N )\n".to_string());
    result_string.push_str(&format!("\t\tinvariant i <= N && i >= {}\n", prefix_length)); //TODO input_mem[x] == input[i - x - 1]
    if !field_name.is_empty() {
        result_string.push_str(&format!(
            "\t\tinvariant acc(r.{})\n",
            field_name.join(") && acc(r.")
        ));
    }
    let mut size_invariant = Vec::new();
    for mem in &memory_stream {
        size_invariant.push(format!(
            "|r.{}_mem| == {}",
            mem.get_name(ir),
            mem.memory_size
        ));
    }
    if !size_invariant.is_empty() {
        result_string.push_str(&format!("\t\tinvariant {}\n", size_invariant.join(" && ")));
    }
    for i_m in &memory_stream {
        if !i_m.input {
            continue;
        }
        let name = i_m.get_name(ir);
        result_string.push_str(&format!(
            "\t\tinvariant r.{}_mem == {}[i - {}..i]\n",
            name,
            name,
            i_m.memory_size
        ));
    }
    //GHost Memory
    // TODO
    ctx.set_inline(true);
    ctx.set_iteration(prefix_length);
    ctx.set_loop();
    result_string.push_str("\t\t//Ghost Memory\n");
    //gm size
    for gm in trigger_deppending.iter().chain(ctx.trigger.iter()) {
        let p = &format!("i - {}", gm.shift);
        result_string.push_str(&format!(
            "\t\tinvariant |{}_ghost| == {}\n",
            gm.get_name(ir),
            if gm.shift == 0 { "i" } else { p }
        ));
    }
    //gm - mem relation
    for gm in trigger_deppending.iter() {
        if gm.memory_size == 0 {
            continue;
        }
        let name = gm.get_name(ir);
        result_string.push_str(&format!(
            "\t\tinvariant {}_ghost[i-{} .. i-{}] == r.{}_mem\n",
            name,
            gm.shift + gm.memory_size,
            gm.shift,
            name,
        ));
    }

    for idx in 0..ctx.trigger.len() {
        let trigger = &ctx.trigger[idx].clone();
        let dep_for_ghost_mem = &dep_per_trigger[idx]; // ctx.get_inline_dep(&trigger.stream_ref);
        let input_dep: Vec<OffsetStream> = dep_for_ghost_mem
            .iter()
            .filter(|s| s.input)
            .cloned()
            .collect();
        let input_dep_strings: Vec<String> =
            input_dep.iter().map(|s| s.get_name(ir).clone()).collect();

        //let (braces_l, braces_r) = if input_dep_strings.is_empty() {("", "")} else {(" {", "}")};

        result_string.push_str(&format!(
            "\t\tinvariant {}_ghost[{}] == ({})\n", //TODO remove comment
            trigger.get_name(ir), //
            format!("i - {}", <i32>::abs(trigger.shift + 1)),
            ctx.print_exp_by_idx(trigger.idx as usize, inline_depth),
        ))
    }
    ctx.set_inline(false);

    //Loop Body
    result_string += &"\t{\n".to_string();
    ctx.set_iteration(prefix_length);
    ctx.set_loop();
    ctx.divisor.clear();
    let mut last_layer = 0;
    for idx in 0..ir.outputs.len() {
        ctx.set_shift(ctx.output_stream[idx].shift); //FIXME

        let exp_result = ctx.print_exp_by_idx(idx, inline_depth);
        for divisor in &ctx.divisor {
            result_string.push_str(&format!(
                "\t\tassume 0 != {}\n",
                divisor,
            ))
        };
        ctx.divisor.clear();

        if layer_optimization {
            let current_layered_stream = layered_streams[idx].clone();
            let name = current_layered_stream.get_name(ir);
            let current_layer = current_layered_stream.layer;
            let original_idx = current_layered_stream.idx;
            if current_layer > last_layer {
                result_string.push_str(&format!("\t\t//Layer {}\n", current_layer));
                last_layer = current_layer;
            }
            let mut dependency_list = ctx.get_inline_dep(&ctx.output_stream[original_idx].stream_ref, 0, Mode::FUNC);
            let args: Vec<String> = dependency_list.iter().map(|oss| if oss.input{ format!("{}[i]",oss.get_name(ir))} else {oss.get_name(ir)}).collect();

            let memory_list = ctx.get_inline_dep(&ctx.output_stream[original_idx].stream_ref, 1, Mode::DEP);

            result_string.push_str(&format!(
                "\t\t{} := compute_{}({}{}{})\n",
                name,
                name,
                if memory_list.len() > 0 {"r"} else {""},
                if memory_list.len() > 0 && args.len() > 0 {", "} else {""},
                args.join(", "),
            ))
        } else {
            result_string.push_str(&format!(
                "\t\t{} := {}\n",
                ir.outputs[idx].name.clone(),
                exp_result,
            ))
        }
    }
    result_string += "\n";
    result_string += &memory_update(&ctx.input_stream, ir, -1, true);
    result_string += &memory_update(&ctx.output_stream, ir, -1, true);
    result_string += "\n";
    result_string += &ghost_update(&trigger_deppending, ir, max_shift + 1, true);
    result_string += &ghost_update(&ctx.trigger, ir, max_shift + 1, true);

    //GM die 2.
    // TODO

    //Close loop body
    result_string.push_str("\n\t\ti := i +1\n\t}\n");

    //Postfix
    ctx.set_post();
    for iteration in 1..=max_shift {
        result_string.push_str(&format!("\n\t// Iteration N + {}\n", iteration));
        ctx.set_iteration(-iteration);
        for idx in 0..ir.outputs.len() {
            let shift = shift_size[idx + ir.inputs.len()].0 as i32;
            //dbg!(iteration, shift, idx, output[idx].name.clone());
            ctx.set_shift(shift);
            if shift < iteration {
                continue;
            }
            result_string.push_str(&format!(
                "\t{} := {}\n",
                ir.outputs[idx].name.clone(),
                ctx.print_exp_by_idx(idx, inline_depth)
            ))
        }

        //Update memory
        result_string.push_str("\n");
        result_string.push_str(&memory_update(&ctx.input_stream, ir, -iteration - 1, false));
        result_string.push_str(&memory_update(
            &ctx.output_stream,
            ir,
            -iteration - 1,
            false,
        ));
        result_string.push_str("\n");
        result_string.push_str(&ghost_update(&trigger_deppending, ir, max_shift + 1, false));
        result_string.push_str(&ghost_update(&ctx.trigger, ir, max_shift + 1, false));


        ctx.set_inline(true);
        result_string +="\n";
        for idx in 0..ctx.trigger.len() {
            let tr = ctx.trigger[idx].clone();
            ctx.set_shift(tr.shift);
            if ctx.get_shift() < iteration {
                continue;
            }
            result_string.push_str(&format!(
                "\tassert {}_ghost[N - {}] == {}\n",
                tr.get_name(ir),
                tr.shift - iteration + 1,
                ctx.print_exp_by_idx(tr.idx, inline_depth)
            ))

        }
        ctx.set_inline(false);
    }

    result_string.push_str("}\n");

    if !ctx.get_functions().is_empty() {
        let mut fun = ctx.get_functions();
        fun.sort();
        fun.dedup();
        let mut function_string = fun.join("\n\n");
        function_string += &"\n\n";
        function_string += &result_string;
        return function_string;
    }

    result_string
}

fn type_string(ty: &Type) -> String {
    match ty {
        Bool => "Bool",
        Int(_) | UInt(_) => "Int",
        Float(_) => "Rational",
        _ => unimplemented!("Only basic type input implemented, got: {}", ty),
    }
    .to_string()
}

fn memory_update(streams: &[OffsetStream], ir: &LolaIR, iteration: i32, indent: bool) -> String {
    let mut s = "".to_string();
    for stream in streams {
        let size = stream.memory_size;
        if size == 0 || iteration > -1 && iteration < stream.shift {
            continue;
        }
        if indent {
            s += "\t";
        }
        let name = stream.get_name(ir);
        let push = if !stream.input {
            name.clone()
        } else if iteration == -1 {
            format!("{}[{}]", name, "i")
        } else {
            format!("{}[{}]", name, iteration)
        };
        if iteration < -1 {
            let it = iteration + 1;
            if -it <= stream.shift {
                s.push_str(&format!(
                    "\tr.{}_mem := r.{}_mem[1..] ++ Seq({})\n",
                    name, name, push
                ));
            } else if -it <= stream.memory_size {
                //s.push_str(&format!("\tr.{}_mem := r.{}_mem[..(|r.{}_mem| - 1)]\n", name, name,name));
                s.push_str(&format!("\tr.{}_mem := r.{}_mem[1..]\n", name, name));
            }
            continue;
        }
        if size > iteration - stream.shift && iteration != -1 {
            s.push_str(&format!(
                "\tr.{}_mem := r.{}_mem ++ Seq({})\n",
                name, name, push
            ));
        } else {
            //size <= iteration
            s.push_str(&format!(
                "\tr.{}_mem := r.{}_mem[1..] ++ Seq({})\n",
                name, name, push
            ));
        }
    }
    s.to_string()
}

fn ghost_update(streams: &[OffsetStream], ir: &LolaIR, iteration: i32, indent: bool) -> String {
    let mut s = "".to_string();
    for stream in streams {
        if stream.input || stream.shift > iteration {
            continue;
        }
        if indent {
            s += "\t";
        }
        let name = stream.get_name(ir);
        s.push_str(&format!(
            "\t{}_ghost := {}_ghost ++ Seq({})\n",
            name, name, name
        ));
    }
    s
}
