use super::*;
use matrix::prelude::Compressed;

#[derive(Debug, Clone)]
struct Node {
    idx: usize,
    out_edges: Vec<(usize, i32, i32)>, // ID, MAX, MIN edge
}

#[derive(Debug, Clone)]
pub struct Graph {
    size: usize,
    nodes: Vec<Node>,
}

impl Graph {
    pub fn new(ir: &LolaIR) -> Self {
        let mut v = Vec::new();
        let mut idx = 0;
        for _i in ir.inputs.iter() {
            v.push(Node {
                idx,
                out_edges: Vec::new(),
            });
            idx += 1;
        }
        let in_len = idx;

        for o in ir.outputs.iter() {
            v.push(Node {
                idx,
                out_edges: {
                    let mut v_o = Vec::new();
                    let d = &o.outgoing_dependencies;
                    for dep in d.iter() {
                        v_o.push((
                            match dep.stream {
                                StreamReference::InRef(x) => x,
                                StreamReference::OutRef(x) => x + in_len,
                            },
                            match dep.offsets.iter().max() {
                                Some(front::ir::Offset::PastDiscreteOffset(x)) => -(*x as i32),
                                Some(front::ir::Offset::FutureDiscreteOffset(x)) => (*x as i32),
                                None => 0,
                                _ => unimplemented!("non discrete offset - graph"),
                            },
                            match dep.offsets.iter().min() {
                                Some(front::ir::Offset::PastDiscreteOffset(x)) => -(*x as i32),
                                Some(front::ir::Offset::FutureDiscreteOffset(x)) => (*x as i32),
                                None => 0,
                                _ => unimplemented!("non discrete offset - graph"),
                            },
                        ))
                    }
                    v_o
                },
            });
            idx += 1;
        }
        Graph {
            size: idx,
            nodes: v,
        }
    }

    pub fn shift(&self) -> Vec<(u32, u32)> {
        //returns shift, memory size
        let shift = self.floyd_warshall();

        let mut mem = shift.clone();

        for (i, n) in self.nodes.iter().enumerate() {
            for ed in &n.out_edges {
                if -ed.2 + shift[i] > mem[ed.0] {
                    mem[ed.0] = shift[i] - ed.2;
                }
            }
        }

        mem.iter()
            .zip(shift)
            .map(|(m, s)| (s as u32, (m - s) as u32))
            .collect::<Vec<(u32, u32)>>()
    }

    pub fn floyd_warshall(&self) -> Vec<i32> {
        use matrix::prelude::*;

        let mut max = Compressed::zero(self.size);

        for i in 0..self.size {
            for j in 0..self.size {
                max.set((i, j), <i32>::min_value() / 10);
            }
        }

        for i in 0..self.size {
            for j in &self.nodes[i].out_edges {
                max.set((j.0, i), j.1);
            }
        }

        for k in 0..self.size {
            for i in 0..self.size {
                for j in 0..self.size {
                    if (get(&max, i, j)) < (get(&max, i, k)) + (get(&max, k, j)) {
                        max.set((i, j), get(&max, i, k) + get(&max, k, j));
                    }
                }
            }
        }

        let mut result = Vec::new();
        let val = &max.values;
        for idx in 0..self.size {
            let m = val[(self.size * (idx))..(self.size * (idx + 1))]
                .iter()
                .max()
                .unwrap();
            result.push(if *m < 0 { 0 } else { *m });
        }
        result
    }

    pub fn layer(&self, shift: Vec<u32>) -> Vec<i32>{
        let mut  layer_vec = vec![0; self.size];

        loop {
            let mut change = false;

            for (idx,node) in self.nodes.iter().enumerate() {
                for (target, max, min) in node.out_edges.iter() {
                    if shift[*target] as i32 == shift[idx] as i32 - max {
                        if layer_vec[idx] <= layer_vec[*target] {
                            layer_vec[idx] = layer_vec[*target] + 1;
                            change = true;
                        }
                    }
                }
            }

            if !change{
                break;
            }
        }
        layer_vec
    }
}

fn get<T: matrix::Element>(m: &Compressed<T>, r: usize, c: usize) -> T {
    assert!(r < m.rows && c < m.columns);
    m.values[m.rows * c + r]
}
