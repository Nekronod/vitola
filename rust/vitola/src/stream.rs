use super::*;
use front::ir::Offset::*;
use front::ir::StreamReference;
use front::ir::{InputStream, Offset, OutputStream};

#[allow(dead_code)]
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct OffsetStream {
    pub input: bool,
    pub idx: usize,
    pub shift: i32,
    pub trigger: bool,
    pub memory_size: i32,
    pub layer: i32,
    pub stream_ref: StreamReference,
    pub t: Type,
}

impl OffsetStream {
    pub fn is_trigger(&self) -> bool {
        self.trigger
    }

    pub fn get_idx(&self) -> usize {
        self.idx
    }

    pub fn get_stream_ref(&self) -> StreamReference {
        self.stream_ref
    }

    pub fn get_name(&self, ir: &LolaIR) -> String {
        if self.input {
            ir.get_in(self.stream_ref).name.clone()
        } else {
            ir.get_out(self.stream_ref).name.clone()
        }
    }

    pub fn get_access_point(&self, shift_accessing: i32, off: Offset) -> i32 {
        let idx: i32 = match off {
            PastDiscreteOffset(x) => (self.shift) - (x as i32) - (shift_accessing),
            FutureDiscreteOffset(x) => (self.shift) + (x as i32) - (shift_accessing),
            FutureRealTimeOffset(_) => unimplemented!("non discrete access"),
            PastRealTimeOffset(_) => unimplemented!("non discrete access"),
        };
        idx
    }
/*
    pub fn add_dependency(&mut self, depends_on: usize){
        if self.depends.contains(&depends_on){
            return;
        } else {
            self.depends.push(depends_on);
        }
    }

    pub fn get_params(&self, ir :&LolaIR) -> Vec<String>{
        let result = self.depends.map(ir.).collect();
        //TODO

    }
    */
}

pub fn from_input(i: &InputStream, size: i32) -> OffsetStream {
    from_stream(true, Some(i), None, 0, size, 0)
}
pub fn from_output(o: &OutputStream, shift: i32, size: i32, layer: i32) -> OffsetStream {
    from_stream(false, None, Some(o), shift, size, layer)
}

fn from_stream(
    flag: bool,
    input: Option<&InputStream>,
    output: Option<&OutputStream>,
    shift: i32,
    memory_size: i32,
    layer: i32,
) -> OffsetStream {
    if flag {
        match input {
            Some(i) => OffsetStream {
                input: true,
                idx: match i.reference { StreamReference::InRef(x) => x, _ => unreachable!("invalid Streamref Match - fromStream")},
                shift: 0,
                trigger: false,
                memory_size,
                layer,
                stream_ref: i.reference,
                t: i.ty.clone(),
            },
            None => unreachable!(),
        }
    } else {
        match output {
            Some(o) => OffsetStream {
                input: false,
                idx: match o.reference { StreamReference::OutRef(x) => x, _ => unreachable!("invalid Streamref Match - fromStream")},
                shift,
                trigger: false,
                memory_size,
                layer,
                stream_ref: o.reference,
                t: o.ty.clone(),
            },
            None => unreachable!(),
        }
    }
}

impl Ord for OffsetStream {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.stream_ref.cmp(&other.stream_ref)
    }
}

impl PartialOrd for OffsetStream {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.stream_ref.cmp(&other.stream_ref))
    }
}
