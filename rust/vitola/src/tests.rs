use super::*;
use crate::expr::Context;
use crate::shift::*;
use front::ir::LolaIR;

fn parse(spec: &str) -> Result<LolaIR, String> {
    front::parse("stdin", spec, front::FrontendConfig::default())
}

#[test]
fn simple() {
    let lola: Result<LolaIR, String> = parse(
        "input a: Int32\n\
         output b: Int32 := a + 1\n\
         output d: Int32 := b.offset(by:-2).defaults(to:0)\n\
         output c: Int32 := b.offset(by:-1).defaults(to:0)",
    );
    assert!(lola.is_ok());
}

#[test]
fn simple_print() {
    let lola= parse(
        "input a: Int32\n\
          output b: Int32 := 5 + 1 + a + a.offset(by:-1).defaults(to:0) + a.offset(by:-2).defaults(to:0)\n").expect("valid");
    //FIXME
}

#[test]
fn simple_print_2() {
    let lola = parse(
        "input a: Int32\n\
         output b: Int32 := 5 + 1 + a + a.offset(by:-1).defaults(to:0)\
         output c: Int32 := b.offset(by:-1).defaults(to:0) + a.offset(by:-3).defaults(to:0)\
         output d: Int32 := c + 2 - c.offset(by:-1).defaults(to:0)\
         \n",
    )
    .expect("valid");
    //FIXME
    /*    let mut ctx =
    ctx.set_iteration(0);
    let b = ctx.print_exp_by_idx(0);
    let c = ctx.print_exp_by_idx(1);
    let d = ctx.print_exp_by_idx(2);
    println!("{}\n{}\n{}", b, c, d);*/
}

#[test]
fn test_translate() {
    let lola= parse(
        "input a: Int32\n\
          output b: Int32 := 5 + 1 + a + a.offset(by:-1).defaults(to:0) + a.offset(by:-2).defaults(to:0)\n").expect("valid");
    println!("{}", translate(&lola));
}


#[test]
fn test_paper_graph() {
    let lola = parse(
        "input t1: Bool\n\
input t2: Int32\n\
output s1 := t1.offset(by:1).defaults(to:false) && s3.offset(by:-7).defaults(to:false)\n\
output s2 := if s1.offset(by:2).defaults(to:true) then t2.offset(by:1).defaults(to:0) else t2.offset(by:-1).defaults(to:2)\n\
output s3 := (s2.offset(by:4).defaults(to:0) <= 5)\n\
trigger s3 \"Error detected\" "
    ).expect("valid");
    let shift = Graph::new(&lola).shift();
    assert_eq!(shift, vec![(0,0),(0,4),(1,0),(3,0),(7,1),(7,0)]);
    let shift2 = shift.iter().map(|t| t.0).collect();
    assert_eq!(Graph::new(&lola).layer(shift2), vec![0,0,1,2,3,4]);

    println!("{}", translate(&lola));
}

#[test]
fn test_translate2() {
    let lola= parse(
        "input a: Int32\n\
          input a2: Bool\n\
          output b: Int32 := 5 + 1 + a + a.offset(by:-1).defaults(to:0) + a.offset(by:-2).defaults(to:0)\n\
          output c: Bool := ! a2.offset(by:-2).defaults(to:false)\n\
          output d: Int32 := a + b.offset(by:-1).defaults(to:0)\n\
          ").expect("valid");
    println!("{}", translate(&lola));
}

#[test]
fn simple_linear() {
    let lola: Result<LolaIR, String> = parse(
        "input a: Int32\n\
         output b: Int32 := a + 1\n\
         output c: Int32 := 1 + b.offset(by:-1).defaults(to:0)\n\
         output d: Int32 := 2 + c.offset(by:-1).defaults(to:0)\n\
         output e: Int32 := 1 + d.offset(by:-1).defaults(to:0)",
    );
    assert!(lola.is_ok());
}

#[test]
fn future_ref_loop() {
    let lola: Result<LolaIR, String> = parse(
        "input a: Int32\n\
         output b: Int32 := a.offset(by:1).defaults(to:0) + d.offset(by:-5).defaults(to:0)\n\
         output c: Int32 := b.offset(by:1).defaults(to:0) + b.offset(by:-1).defaults(to:0)\n\
         output d: Int32 := c.offset(by:2).defaults(to:0)\n\
         output e: Int32 := 1 + d.offset(by:-1).defaults(to:0)",
    );
    assert!(lola.is_ok());
    println!("{:#?}", lola.unwrap());
}

#[test]
fn trigger_linear_past1() {
    let lola: Result<LolaIR, String> = parse(
        "input a: Int32\n\
         output b: Int32 := a + 1\n\
         output c: Int32 := 1 + b.offset(by:-1).defaults(to:0)\n\
         output d: Int32 := 2 + c.offset(by:-1).defaults(to:0)\n\
         output e: Int32 := 1 + d.offset(by:-1).defaults(to:0)\n\
         trigger (a.offset(by:-1).defaults(to:0) + e) < 5 \"value below zero\"",
    );
    assert!(lola.is_ok());
    println!("{}", translate(&lola.unwrap()));
}

#[test]
fn trigger_linear_past2() {
    let lola: Result<LolaIR, String> = parse(
        "\
         input a: Int32\n\
         input a2: Int32\n\
         output b: Int32 := a.offset(by:-10).defaults(to:0) + 1\n\
         output c: Int32 := 1 + b.offset(by:1).defaults(to:0)\n\
         output d: Int32 := 2 + c.offset(by:-1).defaults(to:0)\n\
         output e := a2 + c.offset(by:-3).defaults(to:0)\n\
         trigger a2 + e < 5 \"value below zero\"",
    );
    assert!(lola.is_ok());
    println!("{:#?}", (&lola.unwrap()));
}

#[test]
fn test_inline_dep_1() {
    let lola: Result<LolaIR, String> = parse(
        "\
         input a: Int32\n\
         input a2: Int32\n\
         output b: Int32 := a.offset(by:-10).defaults(to:0) + 1\n\
         output c: Int32 := 1 + b.offset(by:1).defaults(to:0)\n\
         output d: Int32 := 2 + c.offset(by:-1).defaults(to:0)\n\
         output e := a2 + c.offset(by:-3).defaults(to:0)\n\
         trigger a2.offset(by:-1).defaults(to:0) < a.offset(by:-1).defaults(to:0)\"invalid input\"\n\
         trigger a2.offset(by:-1).defaults(to:0) + e + b < 5 \"value below zero\"",
    );
    assert!(lola.is_ok());
    println!("{}", translate(&lola.unwrap()));
}

#[test]
fn test_inline_dep_deep() {
    let lola: Result<LolaIR, String> = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:-2).defaults(to:0) + 1\n\
         output c: Int32 := b\n\
         output d: Int32 := c + b\n\
         output e: Int32 := d\n\
         trigger e != 2 * b \"invalid computation\"",
    );
    assert!(lola.is_ok());
    println!("{}", translate(&lola.unwrap()));
}

#[test]
fn test_inline_dep_deep_shift() {
    let lola: Result<LolaIR, String> = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:-1).defaults(to:0)\n\
         output c: Int32 := b.offset(by:1).defaults(to:0)\n\
         output d: Int32 := c.offset(by:1).defaults(to:0)\n\
         output e: Int32 := d.offset(by:1).defaults(to:0)\n\
         trigger e != b \"invalid computation\"",
    );
    assert!(lola.is_ok());
    println!("{}", translate(&lola.unwrap()));
}

#[test]
fn future_ref_print() {
    let lola: Result<LolaIR, String> = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:1).defaults(to:0)\n\
         output c: Int32 := b.offset(by:3).defaults(to:0) + b.offset(by:-1).defaults(to:0)\n\
         ",
    );
    assert!(lola.is_ok());
    println!("{:#?}", lola.unwrap());
}

#[test]
fn graph_test_translate() {
    let lola = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:1).defaults(to:0) + a + a.offset(by:-1).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + b.offset(by:-1).defaults(to:0) + b.offset(by:1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    assert_eq!((Graph::new(&lola).shift()), vec![(0, 2), (1, 3), (3, 0)]);

    println!("{}", translate(&lola));
}

#[test]
fn test_postfix_mem_access() {
    let lola = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:3).defaults(to:0) + a.offset(by:-2).defaults(to:0)\n\
         output c: Int32 := a.offset(by:-7).defaults(to:0)\n\
         ",
    )
    .unwrap();
    assert_eq!((Graph::new(&lola).shift()), vec![(0, 7), (3, 0), (0, 0)]);
    println!("{}", translate(&lola));
}

#[test]
fn test_postfix_mem_out_access() {
    let lola = parse(
        "\
         input a: Int32\n\
         output b: Int32 := a.offset(by:2).defaults(to:0)\n\
         output c: Int32 := b.offset(by:-7).defaults(to:0) + b.offset(by:-2).defaults(to:0) + b.offset(by:2).defaults(to:0)\n\
         output d: Int32 := b.offset(by:-12).defaults(to:0)\n\
         ",
    )
        .unwrap();
    assert_eq!(
        (Graph::new(&lola).shift()),
        vec![(0, 0), (2, 10), (4, 0), (0, 0)]
    );
    println!("{}", translate(&lola));
}

#[test]
fn test_translate_graph_shift_past() {
    let lola = parse(
        "\
         input i1: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i1.offset(by:1).defaults(to:0) + b.offset(by:-3).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + c.offset(by:-3).defaults(to:0) + c.offset(by:-4).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + a.offset(by:3).defaults(to:0)\n\
         output e: Int32 := i2 + i2.offset(by:-1).defaults(to:0)\n\
         ",
    )
        .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 0, 1, 3, 5, 0]);
    assert_eq!(
        g.shift(),
        vec![(0, 0), (0, 1), (1, 1), (3, 1), (5, 2), (0, 0)]
    );

    println!("{}", translate(&lola));
}

#[test]
fn floyd_test2() {
    let lola = parse(
        "\
         input i: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + b.offset(by:-4).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + a.offset(by:1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 1, 3]);
}

#[test]
fn floyd_test3() {
    let lola = parse(
        "\
         input i: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + b.offset(by:-3).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + a.offset(by:1).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + i2.offset(by:7).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 0, 1, 3, 7]);
}

#[test]
fn floyd_test4() {
    let lola = parse(
        "\
         input i: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + b.offset(by:-3).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + c.offset(by:-3).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + a.offset(by:-1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 0, 1, 3, 5]);
}

#[test]
fn floyd_test5() {
    let lola = parse(
        "\
         input i: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i.offset(by:-1).defaults(to:0) + b.offset(by:-3).defaults(to:0) + i\n\
         output b: Int32 := a.offset(by:-2).defaults(to:0) + c.offset(by:-3).defaults(to:0)\n\
         output c: Int32 := b.offset(by:-2).defaults(to:0) + a.offset(by:-1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    //assert_eq!(g.floyd_warshall(), vec![0, 0, 0, 0, 0]);
    assert_eq!(g.layer(g.floyd_warshall().iter().map(|s| *s as u32).collect()), vec![0, 0, 1, 0, 0]);
}

#[test]
fn graph_test6() {
    let lola = parse(
        "\
         input i: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + b.offset(by:-3).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + c.offset(by:-3).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + a.offset(by:-3).defaults(to:0) + a.offset(by:-2).defaults(to:0)\n\
         ",
    ).unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 0, 1, 3, 5]);
    assert_eq!(g.shift(), vec![(0, 0), (0, 0), (1, 7), (3, 1), (5, 1)])
    //assert_eq!(g.layer(g.shift.iter().map(|s|s.0)), vec![(0, 0), (0, 0), (1, 7), (3, 1), (5, 1)])
}

#[test]
fn graph_test7() {
    let lola = parse(
        "\
         input i: Int32\n\
         input i2: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + b.offset(by:-3).defaults(to:0)\n\
         output b: Int32 := a.offset(by:2).defaults(to:0) + c.offset(by:-3).defaults(to:0)\n\
         output c: Int32 := b.offset(by:2).defaults(to:0) + a.offset(by:-1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 0, 1, 3, 5]);
    assert_eq!(g.shift(), vec![(0, 0), (0, 0), (1, 5), (3, 1), (5, 1)]);
}

#[test]
fn graph_test8() {
    let lola = parse(
        "\
         input i: Int32\n\
         output a: Int32 := i.offset(by:1).defaults(to:0) + i + i.offset(by:-1).defaults(to:0)\n\
         output b: Int32 := a.offset(by:1).defaults(to:0) + a + a.offset(by:-1).defaults(to:0)\n\
         ",
    )
    .unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.floyd_warshall(), vec![0, 1, 2]);
    assert_eq!(g.shift(), vec![(0, 2), (1, 2), (2, 0)]);
}


#[test]
fn test_layer_prop() {
    let lola = parse("input c: Int32
            input signal: Bool
            output sum: Int32 := c.offset(by:-1).defaults(to:0) + c + c.offset(by:1).defaults(to:0)
            output exp: Bool := !(sum < 5) || (signal.offset(by:2).defaults(to:false) || signal.offset(by:3).defaults(to:false))
            trigger !exp").unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.layer(g.shift().iter().map(|s|s.0).collect()), vec![0, 0, 1, 1, 2])
}


#[test]
fn test_layer_prop_past_stream() {
    let lola = parse("input c: Int32
            input signal: Bool
            output sum: Int32 := c.offset(by:-1).defaults(to:0) + c + c.offset(by:1).defaults(to:0)
            output exp: Bool := !(sum < 5) || (signal.offset(by:2).defaults(to:false) || signal.offset(by:3).defaults(to:false))
            output test: Bool := exp.offset(by:-5).defaults(to:false)
            trigger !exp").unwrap();
    let mut g = Graph::new(&lola);
    assert_eq!(g.shift(), vec![(0, 2), (0, 1), (1, 2), (3, 2), (0, 0), (3, 0)]);
    assert_eq!(g.layer(g.shift().iter().map(|s|s.0).collect()), vec![0, 0, 1, 1, 0, 2])
}


#[test]
fn test_rt() {
    let lola = parse(

        "input puls: float63
            input signal: Bool
            trigger !true").unwrap();
    print!("{:#?}",lola);
}



#[test]
fn test_ordering_offset() {
    use front::ir::Offset::*;
    assert!(FutureDiscreteOffset(2) > FutureDiscreteOffset(1));
    assert!(!(FutureDiscreteOffset(2) > FutureDiscreteOffset(2)));
    assert!((FutureDiscreteOffset(2) == FutureDiscreteOffset(2)));
    assert!(FutureDiscreteOffset(1) > PastDiscreteOffset(10));
    assert!(PastDiscreteOffset(2) < PastDiscreteOffset(1));
}

#[test]
fn test_ordering_ref() {
    use front::ir::StreamReference::*;
    assert!(InRef(2) > InRef(1));
    assert!(!(InRef(2) > InRef(2)));
    assert!((InRef(2) == InRef(2)));
    assert!(InRef(3) < OutRef(1));
    assert!(InRef(2) < OutRef(2));
    assert!(OutRef(3) > OutRef(2));
    assert!(OutRef(3) == OutRef(3));
}
