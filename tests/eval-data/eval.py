from __future__ import division
import os
import time

def timeout_command(command, timeout):
  """call shell-command and either return its output or kill it
  if it doesn't normally exit within timeout seconds and return None"""
  import subprocess, datetime, os, time, signal
  start = datetime.datetime.now()
  process = subprocess.Popen(command,stderr=subprocess.PIPE,stdout=subprocess.PIPE,shell=True)
  while process.poll() is None:
    time.sleep(0.1)
    now = datetime.datetime.now()
    if (now - start).seconds > timeout:
      print("KILLED")
      os.kill(process.pid, signal.SIGKILL)
      os.waitpid(-1, os.WNOHANG)
      return None
  return process.stdout.read()

i=0
dur = []
while(i < 20 ):
    start = time.time()
    os.system('rm tmp -r')
    #a = timeout_command(['time','java', '-jar', '-Xmx8192m','-Xss32m','/usr/local/Viper/backends/silicon.jar',' ~/uni/bachelor/tests/input/drone-org-5.vpr'],10)
    os.system('/usr/bin/time -v java -jar -Xmx8192m  -Xss32m /usr/local/Viper/backends/silicon.jar  ~/uni/bachelor/tests/input/eval4.vpr')
    #print(a)
    end = time.time()
    i+=1
    print(str(i))
    duration = end - start
    print(duration)
    dur.append(duration)

print("Max: "+ str( max(dur)))
print("Min: "+ str(min(dur)))
print("Avg: "+ str(sum(dur)/len(dur)))
