field t1_mem :Seq[Bool]
field s1_mem :Seq[Bool]
function compute_trigger_hard(r: Ref, s1: Bool): Bool
	requires acc(r.t1_mem, 1/3)
	requires |r.t1_mem| == 2
{
	(s1 && (r.t1_mem[1] && r.t1_mem[0]))
}

function compute_trigger_easy(r: Ref, s1: Bool): Bool
	requires acc(r.t1_mem, 1/3)
	requires |r.t1_mem| == 2
{
	(!s1 && !r.t1_mem[1])
}

function compute_s1(r: Ref, t1: Bool): Bool
	requires acc(r.s1_mem, 1/3)
	requires |r.s1_mem| == 1
{
	(t1 || r.s1_mem[0])
}

method monitor (t1: Seq[Bool])
	requires |t1| >= 3
{
	var N: Int := |t1|
	var r: Ref
	r := new(t1_mem, s1_mem)
	r.t1_mem := Seq()
	r.s1_mem := Seq()

	var s1: Bool
	var trigger_easy: Bool
	var trigger_hard: Bool
	var s1_ghost: Seq[Bool] := Seq()
	var trigger_easy_ghost: Seq[Bool] := Seq()
	var trigger_hard_ghost: Seq[Bool] := Seq()


	// Iteration 0

	r.t1_mem := r.t1_mem ++ Seq(t1[0])



	// Iteration 1
	s1 := (t1[1] || false)
	trigger_easy := (!s1 && !r.t1_mem[0])
	trigger_hard := (s1 && (r.t1_mem[0] && true))

	r.t1_mem := r.t1_mem ++ Seq(t1[1])
	r.s1_mem := r.s1_mem ++ Seq(s1)

	s1_ghost := s1_ghost ++ Seq(s1)
	trigger_easy_ghost := trigger_easy_ghost ++ Seq(trigger_easy)
	trigger_hard_ghost := trigger_hard_ghost ++ Seq(trigger_hard)

	assert trigger_easy_ghost[0] == (!(t1[1] || false) && !t1[0])
	assert trigger_hard_ghost[0] == ((t1[1] || false) && (t1[0] && true))

	// Iteration 2
	s1 := (t1[2] || r.s1_mem[0])
	trigger_easy := (!s1 && !r.t1_mem[1])
	trigger_hard := (s1 && (r.t1_mem[1] && r.t1_mem[0]))

	r.t1_mem := r.t1_mem[1..] ++ Seq(t1[2])
	r.s1_mem := r.s1_mem[1..] ++ Seq(s1)

	s1_ghost := s1_ghost ++ Seq(s1)
	trigger_easy_ghost := trigger_easy_ghost ++ Seq(trigger_easy)
	trigger_hard_ghost := trigger_hard_ghost ++ Seq(trigger_hard)

	assert trigger_easy_ghost[1] == (!(t1[2] || s1_ghost[0]) && !t1[1])
	assert trigger_hard_ghost[1] == ((t1[2] || s1_ghost[0]) && (t1[1] && t1[0]))

	var i: Int := 3
	while( i < N )
		invariant i <= N && i >= 3
		invariant acc(r.t1_mem) && acc(r.s1_mem)
		invariant |r.t1_mem| == 2 && |r.s1_mem| == 1
		invariant r.t1_mem == t1[i - 2..i]
		//Ghost Memory
		invariant |s1_ghost| == i - 1
		invariant |trigger_easy_ghost| == i - 1
		invariant |trigger_hard_ghost| == i - 1
		invariant s1_ghost[i-2 .. i-1] == r.s1_mem
		invariant trigger_easy_ghost[i - 2] == ((!(t1[i - 1] || s1_ghost[i - 3]) && !t1[i - 2]))
		invariant trigger_hard_ghost[i - 2] == (((t1[i - 1] || s1_ghost[i - 3]) && (t1[i - 2] && t1[i - 3])))
	{
		//Layer 1
		s1 := compute_s1(r, t1[i])
		//Layer 2
		trigger_easy := compute_trigger_easy(r, s1)
		trigger_hard := compute_trigger_hard(r, s1)

		r.t1_mem := r.t1_mem[1..] ++ Seq(t1[i])
		r.s1_mem := r.s1_mem[1..] ++ Seq(s1)

		s1_ghost := s1_ghost ++ Seq(s1)
		trigger_easy_ghost := trigger_easy_ghost ++ Seq(trigger_easy)
		trigger_hard_ghost := trigger_hard_ghost ++ Seq(trigger_hard)

		i := i +1
	}

	// Iteration N + 1
	s1 := (false || r.s1_mem[0])
	trigger_easy := (!s1 && !r.t1_mem[1])
	trigger_hard := (s1 && (r.t1_mem[1] && r.t1_mem[0]))

	r.t1_mem := r.t1_mem[1..]
	r.s1_mem := r.s1_mem[1..] ++ Seq(s1)

	s1_ghost := s1_ghost ++ Seq(s1)
	trigger_easy_ghost := trigger_easy_ghost ++ Seq(trigger_easy)
	trigger_hard_ghost := trigger_hard_ghost ++ Seq(trigger_hard)

	assert trigger_easy_ghost[N - 1] == (!(false || s1_ghost[N - 2]) && !t1[N - 1])
	assert trigger_hard_ghost[N - 1] == ((false || s1_ghost[N - 2]) && (t1[N - 1] && t1[N - 2]))
}
