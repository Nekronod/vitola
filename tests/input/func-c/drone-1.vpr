field lat_mem :Seq[Int]
field lon_mem :Seq[Int]
field time_mem :Seq[Int]
field count_mem :Seq[Int]
field freq_sum_mem :Seq[Int]
function compute_trigger_Jump(detected_jump: Bool): Bool
{
	detected_jump
}

function compute_detected_jump(dif_distance: Int, delta_distance: Int): Bool
{
	(dif_distance > delta_distance)
}

function compute_delta_distance(): Int
{
	10
}

function compute_dif_distance(gps_distance: Int, distance_max: Int): Int
{
	(gps_distance - distance_max)
}

function compute_distance_max(velocity: Int, passed_time: Int): Int
{
	(velocity * passed_time)
}

function compute_passed_time(r: Ref, time: Int): Int
	requires acc(r.time_mem, 1/26)
	requires |r.time_mem| == 1
{
	(time - r.time_mem[0])
}

function compute_gps_distance(R: Int, c: Int): Int
{
	(R * c)
}

function compute_c(a: Int): Int
{
	(2 * ((a * a) + ((a / 2) * (a / 2))))
}

function compute_a(lat1_rad: Int, lat2_rad: Int, dlon: Int, dlat: Int): Int
{
	((((((dlat / 2) * (dlat / 2)) * lat1_rad) * lat2_rad) * (dlon / 2)) * (dlon / 2))
}

function compute_dlat(lat1_rad: Int, lat2_rad: Int): Int
{
	(lat2_rad - lat1_rad)
}

function compute_dlon(lon1_rad: Int, lon2_rad: Int): Int
{
	(lon2_rad - lon1_rad)
}

function compute_lat2_rad(lat: Int, pi: Int): Int
{
	((lat * pi) / 180)
}

function compute_lat1_rad(r: Ref, pi: Int): Int
	requires acc(r.lat_mem, 1/26)
	requires |r.lat_mem| == 1
{
	((r.lat_mem[0] * pi) / 180)
}

function compute_lon2_rad(lon: Int, pi: Int): Int
{
	((lon * pi) / 180)
}

function compute_lon1_rad(r: Ref, pi: Int): Int
	requires acc(r.lon_mem, 1/26)
	requires |r.lon_mem| == 1
{
	((r.lon_mem[0] * pi) / 180)
}

function compute_pi(): Int
{
	3
}

function compute_R(): Int
{
	6373000
}

function compute_velocity(ug: Int, vg: Int, wg: Int): Int
{
	(((ug * ug) + (vg * vg)) + (wg * wg))
}

function compute_freq_min(frequency: Int): Int
{
	frequency
}

function compute_freq_max(frequency: Int): Int
{
	frequency
}

function compute_freq_avg(count: Int, freq_sum: Int): Int
{
	(freq_sum / count)
}

function compute_freq_sum(r: Ref, frequency: Int): Int
	requires acc(r.freq_sum_mem, 1/26)
	requires |r.freq_sum_mem| == 1
{
	(frequency + r.freq_sum_mem[0])
}

function compute_frequency(r: Ref, time: Int): Int
	requires acc(r.time_mem, 1/26)
	requires |r.time_mem| == 1
{
	(1 / (time - r.time_mem[0]))
}

function compute_flight_time(time: Int): Int
{
	time
}

function compute_count(r: Ref): Int
	requires acc(r.count_mem, 1/26)
	requires |r.count_mem| == 1
{
	(r.count_mem[0] + 1)
}

function compute_time(time_s: Int, time_micors: Int): Int
{
	(time_s + (time_micors / 1000000))
}

method monitor (lat: Seq[Int], lon: Seq[Int], ug: Seq[Int], vg: Seq[Int], wg: Seq[Int], time_s: Seq[Int], time_micors: Seq[Int])
	requires |lat| == |lon|
	requires |lon| == |ug|
	requires |ug| == |vg|
	requires |vg| == |wg|
	requires |wg| == |time_s|
	requires |time_s| == |time_micors|
	requires |lat| >= 2
{
	var N: Int := |lat|
	var r: Ref
	r := new(lat_mem, lon_mem, time_mem, count_mem, freq_sum_mem)
	r.lat_mem := Seq()
	r.lon_mem := Seq()
	r.time_mem := Seq()
	r.count_mem := Seq()
	r.freq_sum_mem := Seq()

	var time: Int
	var count: Int
	var flight_time: Int
	var frequency: Int
	var freq_sum: Int
	var freq_avg: Int
	var freq_max: Int
	var freq_min: Int
	var velocity: Int
	var R: Int
	var pi: Int
	var lon1_rad: Int
	var lon2_rad: Int
	var lat1_rad: Int
	var lat2_rad: Int
	var dlon: Int
	var dlat: Int
	var a: Int
	var c: Int
	var gps_distance: Int
	var passed_time: Int
	var distance_max: Int
	var dif_distance: Int
	var delta_distance: Int
	var detected_jump: Bool
	var trigger_Jump: Bool
	var time_ghost: Seq[Int] := Seq()
	var trigger_Jump_ghost: Seq[Bool] := Seq()


	// Iteration 0
	 assume 0 != (time - r.time_mem[0])
	 assume 0 != count
	time := (time_s[0] + (time_micors[0] / 1000000))
	count := (0 + 1)
	flight_time := time
	 assume 0 != (time - 0)
	frequency := (1 / (time - 0))
	freq_sum := (frequency + 0)
	 assume 0 != count
	freq_avg := (freq_sum / count)
	freq_max := frequency
	freq_min := frequency
	velocity := (((ug[0] * ug[0]) + (vg[0] * vg[0])) + (wg[0] * wg[0]))
	R := 6373000
	pi := 3
	lon1_rad := ((0 * pi) / 180)
	lon2_rad := ((lon[0] * pi) / 180)
	lat1_rad := ((0 * pi) / 180)
	lat2_rad := ((lat[0] * pi) / 180)
	dlon := (lon2_rad - lon1_rad)
	dlat := (lat2_rad - lat1_rad)
	a := ((((((dlat / 2) * (dlat / 2)) * lat1_rad) * lat2_rad) * (dlon / 2)) * (dlon / 2))
	c := (2 * ((a * a) + ((a / 2) * (a / 2))))
	gps_distance := (R * c)
	passed_time := (time - 0)
	distance_max := (velocity * passed_time)
	dif_distance := (gps_distance - distance_max)
	delta_distance := 10
	detected_jump := (dif_distance > delta_distance)
	trigger_Jump := detected_jump

	r.lat_mem := r.lat_mem ++ Seq(lat[0])
	r.lon_mem := r.lon_mem ++ Seq(lon[0])
	r.time_mem := r.time_mem ++ Seq(time)
	r.count_mem := r.count_mem ++ Seq(count)
	r.freq_sum_mem := r.freq_sum_mem ++ Seq(freq_sum)

	time_ghost := time_ghost ++ Seq(time)
	trigger_Jump_ghost := trigger_Jump_ghost ++ Seq(trigger_Jump)

	assert trigger_Jump_ghost[0] == (((6373000 * (2 * (((((((((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2) * ((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((0 * 3) / 180)) * ((lat[0] * 3) / 180)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * (((((((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2) * ((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((0 * 3) / 180)) * ((lat[0] * 3) / 180)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2))) + (((((((((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2) * ((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((0 * 3) / 180)) * ((lat[0] * 3) / 180)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) / 2) * ((((((((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2) * ((((lat[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((0 * 3) / 180)) * ((lat[0] * 3) / 180)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) * ((((lon[0] * 3) / 180) - ((0 * 3) / 180)) / 2)) / 2))))) - ((((ug[0] * ug[0]) + (vg[0] * vg[0])) + (wg[0] * wg[0])) * ((time_s[0] + (time_micors[0] / 1000000)) - 0))) > 10)

	// Iteration 1
	time := (time_s[1] + (time_micors[1] / 1000000))
	count := (r.count_mem[0] + 1)
	flight_time := time
	 assume 0 != (time - r.time_mem[0])
	frequency := (1 / (time - r.time_mem[0]))
	freq_sum := (frequency + r.freq_sum_mem[0])
	 assume 0 != count
	freq_avg := (freq_sum / count)
	freq_max := frequency
	freq_min := frequency
	velocity := (((ug[1] * ug[1]) + (vg[1] * vg[1])) + (wg[1] * wg[1]))
	R := 6373000
	pi := 3
	lon1_rad := ((r.lon_mem[0] * pi) / 180)
	lon2_rad := ((lon[1] * pi) / 180)
	lat1_rad := ((r.lat_mem[0] * pi) / 180)
	lat2_rad := ((lat[1] * pi) / 180)
	dlon := (lon2_rad - lon1_rad)
	dlat := (lat2_rad - lat1_rad)
	a := ((((((dlat / 2) * (dlat / 2)) * lat1_rad) * lat2_rad) * (dlon / 2)) * (dlon / 2))
	c := (2 * ((a * a) + ((a / 2) * (a / 2))))
	gps_distance := (R * c)
	passed_time := (time - r.time_mem[0])
	distance_max := (velocity * passed_time)
	dif_distance := (gps_distance - distance_max)
	delta_distance := 10
	detected_jump := (dif_distance > delta_distance)
	trigger_Jump := detected_jump

	r.lat_mem := r.lat_mem[1..] ++ Seq(lat[1])
	r.lon_mem := r.lon_mem[1..] ++ Seq(lon[1])
	r.time_mem := r.time_mem[1..] ++ Seq(time)
	r.count_mem := r.count_mem[1..] ++ Seq(count)
	r.freq_sum_mem := r.freq_sum_mem[1..] ++ Seq(freq_sum)

	time_ghost := time_ghost ++ Seq(time)
	trigger_Jump_ghost := trigger_Jump_ghost ++ Seq(trigger_Jump)

	assert trigger_Jump_ghost[1] == (((6373000 * (2 * (((((((((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2) * ((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2)) * ((lat[0] * 3) / 180)) * ((lat[1] * 3) / 180)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) * (((((((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2) * ((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2)) * ((lat[0] * 3) / 180)) * ((lat[1] * 3) / 180)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2))) + (((((((((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2) * ((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2)) * ((lat[0] * 3) / 180)) * ((lat[1] * 3) / 180)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) / 2) * ((((((((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2) * ((((lat[1] * 3) / 180) - ((lat[0] * 3) / 180)) / 2)) * ((lat[0] * 3) / 180)) * ((lat[1] * 3) / 180)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) * ((((lon[1] * 3) / 180) - ((lon[0] * 3) / 180)) / 2)) / 2))))) - ((((ug[1] * ug[1]) + (vg[1] * vg[1])) + (wg[1] * wg[1])) * ((time_s[1] + (time_micors[1] / 1000000)) - time_ghost[0]))) > 10)

	var i: Int := 2
	while( i < N )
		invariant i <= N && i >= 2
		invariant acc(r.lat_mem) && acc(r.lon_mem) && acc(r.time_mem) && acc(r.count_mem) && acc(r.freq_sum_mem)
		invariant |r.lat_mem| == 1 && |r.lon_mem| == 1 && |r.time_mem| == 1 && |r.count_mem| == 1 && |r.freq_sum_mem| == 1
		invariant r.lat_mem == lat[i - 1..i]
		invariant r.lon_mem == lon[i - 1..i]
		//Ghost Memory
		invariant |time_ghost| == i
		invariant |trigger_Jump_ghost| == i
		invariant time_ghost[i-1 .. i-0] == r.time_mem
		invariant trigger_Jump_ghost[i - 1] == ((((6373000 * (2 * (((((((((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2) * ((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2)) * ((lat[i - 2] * 3) / 180)) * ((lat[i - 1] * 3) / 180)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) * (((((((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2) * ((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2)) * ((lat[i - 2] * 3) / 180)) * ((lat[i - 1] * 3) / 180)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2))) + (((((((((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2) * ((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2)) * ((lat[i - 2] * 3) / 180)) * ((lat[i - 1] * 3) / 180)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) / 2) * ((((((((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2) * ((((lat[i - 1] * 3) / 180) - ((lat[i - 2] * 3) / 180)) / 2)) * ((lat[i - 2] * 3) / 180)) * ((lat[i - 1] * 3) / 180)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) * ((((lon[i - 1] * 3) / 180) - ((lon[i - 2] * 3) / 180)) / 2)) / 2))))) - ((((ug[i - 1] * ug[i - 1]) + (vg[i - 1] * vg[i - 1])) + (wg[i - 1] * wg[i - 1])) * ((time_s[i - 1] + (time_micors[i - 1] / 1000000)) - time_ghost[i - 2]))) > 10))
	{
		count := compute_count(r)
		R := compute_R()
		pi := compute_pi()
		assume 0 != (time - r.time_mem[0])
		delta_distance := compute_delta_distance()
		//Layer 1
		time := compute_time(time_s[i], time_micors[i])
		assume 0 != count
		velocity := compute_velocity(ug[i], vg[i], wg[i])
		lon1_rad := compute_lon1_rad(r, pi)
		lon2_rad := compute_lon2_rad(lon[i], pi)
		lat1_rad := compute_lat1_rad(r, pi)
		lat2_rad := compute_lat2_rad(lat[i], pi)
		//Layer 2
		flight_time := compute_flight_time(time)
		frequency := compute_frequency(r, time)
		dlon := compute_dlon(lon1_rad, lon2_rad)
		dlat := compute_dlat(lat1_rad, lat2_rad)
		passed_time := compute_passed_time(r, time)
		//Layer 3
		freq_sum := compute_freq_sum(r, frequency)
		freq_max := compute_freq_max(frequency)
		freq_min := compute_freq_min(frequency)
		a := compute_a(lat1_rad, lat2_rad, dlon, dlat)
		distance_max := compute_distance_max(velocity, passed_time)
		//Layer 4
		freq_avg := compute_freq_avg(count, freq_sum)
		c := compute_c(a)
		//Layer 5
		gps_distance := compute_gps_distance(R, c)
		//Layer 6
		dif_distance := compute_dif_distance(gps_distance, distance_max)
		//Layer 7
		detected_jump := compute_detected_jump(dif_distance, delta_distance)
		//Layer 8
		trigger_Jump := compute_trigger_Jump(detected_jump)

		r.lat_mem := r.lat_mem[1..] ++ Seq(lat[i])
		r.lon_mem := r.lon_mem[1..] ++ Seq(lon[i])
		r.time_mem := r.time_mem[1..] ++ Seq(time)
		r.count_mem := r.count_mem[1..] ++ Seq(count)
		r.freq_sum_mem := r.freq_sum_mem[1..] ++ Seq(freq_sum)

		time_ghost := time_ghost ++ Seq(time)
		trigger_Jump_ghost := trigger_Jump_ghost ++ Seq(trigger_Jump)

		i := i +1
	}
}
